var express = require("express");
var app = express();

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(express.static('public'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

var server = require("http").Server(app);
var io = require("socket.io")(server);
var port = 9090;
server.listen(process.env.PORT || port , function(){
	console.log('server started on port 9090')
});

//bật kết nối socket
io.on("connection", function(socket){
	console.log("co nguoi ket noi:" + socket.id );
	socket.on("disconnect", function(){
		console.log(socket.id + "Da ngat ket noi");
	});
});

var control = require('./controllers');
app.get('/', control.home);
app.get('/jas', control.jas);